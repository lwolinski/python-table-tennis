import pygame
from table_tennis import *

""" Settings """

window = {"width": 800, "height": 600, "background_color": pygame.color.Color("Black"), "font_color": pygame.color.Color("Green"), "font_size": 32}

playerA_paddle = {"width": 20, "length": 100, "vertical_delta": 1, "color": pygame.color.Color("Green"), "up_key": pygame.K_w, "down_key": pygame.K_s}
playerB_paddle = {"width": 20, "length": 100,"vertical_delta": 1, "color": pygame.color.Color("Green"), "up_key": pygame.K_UP, "down_key": pygame.K_DOWN}

ball = {"size": 20, "horizontal_delta": 0.5, "vertical_delta": 0.5, "bump": 1.05, "color": pygame.color.Color("Green")}

""" Start the game """
main(window, playerA_paddle, playerB_paddle, ball)
