import pygame

""" Table tennis game """

pygame.init()

class Player:
	"""
	Class Player:
	x, y -- location of the player's paddle on the screen
	dy -- number of pixels to move up or down after detecting the keystroke
	width, length -- dimensions of the paddle
	color -- color of the paddle
	up, down -- pygame keys to move up or down
	score -- current score of the player
	vertical_move -- either +dy or -dy
	"""
	
	def __init__(self, x, y, dy, width, length, color, up, down):
		self.x = x
		self.y = y
		self.dy = dy
		self.width = width
		self.length = length
		self.color = color
		self.up = up
		self.down = down
		self.score = 0
		self.vertical_move = 0
	
	def draw(self, screen):
		""" Draw the paddle on the screen """
		pygame.draw.rect( screen, self.color, (self.x, self.y, self.width, self.length) )
	
	def set_movement(self, x):
		""" Set the paddle to move up or down by dy """
		self.vertical_move = self.dy * x
	
	def move(self):
		""" Move the paddle up or down """
		self.y += self.vertical_move
	
	def score_goes_up(self):
		""" Increase the player's score by 1 point """
		self.score += 1
		
	def get_score(self):
		""" Get the player's score """
		return self.score
		
	def get_pos(self):
		""" Get the position of the paddle's top left corner """
		return (self.x, self.y)
		
	def get_width(self):
		""" Get the width of the paddle """
		return self.width
		
	def get_length(self):
		""" Get the length of the paddle """
		return self.length
		
	def get_up(self):
		""" Get the up key """
		return self.up
		
	def get_down(self):
		""" Get the down key """
		return self.down

class Ball:
	"""
	Class Ball:
	x, y -- location of the ball on the screen
	dx, dy -- number of pixels to move along the x and y axis in each time instant
	bump -- parameter describing how fast the dx and dy increments will increase after collisions
	size -- dimensions of the ball are size * size
	color -- color of the ball
	"""
	
	def __init__(self, x, y, dx, dy, bump, size, color):
		self.x = x
		self.y = y
		self.dx = dx
		self.dy = dy
		self.initial_dx = dx
		self.initial_dy = dy
		self.bump = bump
		self.size = size
		self.color = color

	def draw(self, screen):
		""" Draw the ball on the screen """
		pygame.draw.rect( screen, self.color, (self.x, self.y, self.size, self.size) )
	
	def move(self):
		""" Move the ball """
		self.x += self.dx
		self.y += self.dy
	
	def get_size(self):
		""" Get the size of the ball (its top left corner) """
		return self.size
	
	def get_pos(self):
		""" Get the position of the ball """
		return (self.x, self.y)
		
	def set_pos(self, pos):
		""" Set the position of the ball """
		(self.x, self.y) = pos
		
	def get_dy(self):
		""" Get the ball's increment along the y axis """
		return self.dy
	
	def set_dy(self, dy):
		""" Set the increment along the y axis """
		self.dy = dy
	
	def get_dx(self):
		""" Get the ball's increment along the x axis """
		return self.dx
	
	def set_dx(self, dx):
		""" Set the increment along the x axis """
		self.dx = dx
		
	def reverse_dx(self):
		self.dx *= -self.bump
		
	def reverse_dy(self):
		self.dy *= -self.bump

class Board:
	"""
	Class Board
	width, height -- dimensions of the game board
	"""
	
	def __init__(self, width, height):
		self.width = width
		self.height = height
	
	def check_ball_collision(self, ball, left_player, right_player):
		""" Check the collision of the ball with the edges of the board and with the players' paddles """
		
		(x, y) = ball.get_pos()
		
		if (y >= self.height - ball.get_size()) or (y <= 0):
			#~ ball.set_dy( -ball.get_dy() )
			ball.reverse_dy()
		
		if (x >= self.width - ball.get_size()):
			self.reset_ball(ball)
			left_player.score_goes_up()
		
		if (x <= 0):
			self.reset_ball(ball)
			right_player.score_goes_up()
		
		self.check_paddle_collision(ball, left_player)
		self.check_paddle_collision(ball, right_player)
		
	def check_paddle_collision(self, ball, player):
		""" Check the collision of the ball with the paddle """
		(x, y) = ball.get_pos()
		x += ball.get_size() // 2
		(X, Y) = player.get_pos()
		if (x > X and x < X + player.get_width()) and (y > Y and y < Y + player.get_length()):
			#~ ball.set_dx( -ball.get_dx() )
			ball.reverse_dx()
	
	def reset_ball(self, ball):
		""" Reset the ball's position and velocity """
		xBall = self.width // 2
		yBall = self.height // 2 - ball.get_size() // 2
		ball.set_pos( (xBall, yBall) )
		ball.set_dx( -ball.initial_dx )
		ball.set_dy( -ball.initial_dy )

class Score:
	""" Class Score defines the score display """
	def __init__(self, font_size, font_color, center_pos):
		self.font_size = font_size
		self.font_color = font_color
		self.center_pos = center_pos
		self.font = pygame.font.Font("freesansbold.ttf", self.font_size)
		
	def print_score(self, screen, playerA_score, playerB_score):
		text = self.font.render("Player A: {}  Player B: {}".format(playerA_score, playerB_score), True, self.font_color)
		text_rect = text.get_rect(center = self.center_pos)
		screen.blit(text, text_rect)

def check_keydown(playerA, playerB, key):
	""" Choose appriopriate action based on the pressed key """
	if key == playerA.get_up():
		playerA.set_movement(-1)
	if key == playerA.get_down():
		playerA.set_movement(1)
	if key == playerB.get_up():
		playerB.set_movement(-1)
	if key == playerB.get_down():
		playerB.set_movement(1)
	
def check_keyup(playerA, playerB, key):
	""" Choose appriopriate action based on the released key """
	if key == playerA.get_up() or key == playerA.get_down():
		playerA.set_movement(0)
	if key == playerB.get_up() or key == playerB.get_down():
		playerB.set_movement(0)

def create_stuff(w, pA, pB, b):
	"""
	Create objects based on the settings of:
	w -- window
	pA -- player A's paddle
	pB -- player B's paddle
	b -- ball
	"""
	# Create the game window
	screen = pygame.display.set_mode( (w["width"], w["height"]) )
	screen.fill( w["background_color"] )
	pygame.display.set_caption("Two paddles, one ball")
	
	# Create the score text
	score = Score(w["font_size"], w["font_color"], (w["width"] // 2, w["font_size"]) )
	
	# Create the game board
	board = Board(w["width"], w["height"])
	
	# Create the players' paddles
	xA = pA["width"]
	yA = w["height"] // 2 - pA["length"] // 2
	playerA = Player(xA, yA, pA["vertical_delta"], pA["width"], pA["length"], pA["color"], pA["up_key"], pA["down_key"])
	xB = w["width"] - 2 * pB["width"]
	yB = w["height"] // 2 - pB["length"] // 2
	playerB = Player(xB, yB, pB["vertical_delta"], pB["width"], pB["length"], pB["color"], pB["up_key"], pB["down_key"])
	
	# Create the ball
	xBall = w["width"] // 2
	yBall = w["height"] // 2 - b["size"] // 2
	ball = Ball(xBall, yBall, b["horizontal_delta"], b["vertical_delta"], b["bump"], b["size"], b["color"])
	
	return screen, score, board, playerA, playerB, ball

def main(window_data, playerA_data, playerB_data, ball_data):
	""" Main program """
	
	# Create the game window, game score text, game board, players' paddles and ball
	screen, score, board, playerA, playerB, ball = create_stuff(window_data, playerA_data, playerB_data, ball_data)

	# Main game loop
	playing = True
	while playing:
		for event in pygame.event.get():
			
			# Aborting the game
			if event.type == pygame.QUIT:
				playing = False
			
			# Checking the pressed key
			if event.type == pygame.KEYDOWN:
				check_keydown(playerA, playerB, event.key)
			# Checking the released key
			if event.type == pygame.KEYUP:
				check_keyup(playerA, playerB, event.key)
		
		# Move the player's paddles
		playerA.move()
		playerB.move()
		
		# Move the ball and check for the collisions
		ball.move()
		board.check_ball_collision(ball, playerA, playerB)
		
		# Paint the screen black
		screen.fill( pygame.color.Color("Black") )
		
		# Draw the players' paddles and the ball on the screen
		playerA.draw(screen)
		playerB.draw(screen)
		ball.draw(screen)
		
		# Print the game score on the screen
		score.print_score(screen, playerA.get_score(), playerB.get_score())
		
		pygame.display.update()
		
	pygame.quit()
